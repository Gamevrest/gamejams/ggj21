using UnityEngine;

public class ExampleSender : MonoBehaviour
{
    public USES.Event voidEvent;
    public USES.Event stringEvent;

    public void TriggerEvent()
    {
        voidEvent.Raise();
    }
    
    public void TriggerEvent(string s)
    {
        stringEvent.Raise(s);
    }

}
