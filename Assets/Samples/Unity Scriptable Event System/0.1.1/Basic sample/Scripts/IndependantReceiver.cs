﻿using UnityEngine;
using USES;

public class IndependantReceiver : MonoBehaviour
{
    public USES.Event eventToSubscribe;

    private void OnEnable()
    {
        eventToSubscribe.Register(new PayloadUnityEvent(Response), gameObject);
    }

    private void OnDisable()
    {
        eventToSubscribe.Unregister(gameObject);
    }

    private void Response(Payload payload)
    {
        print($"received : [{payload.name}] of type [{payload.GetType()}]");
    }
}