using UnityEngine;

public class ExampleReceiver : MonoBehaviour
{
    public void PrintPayload(USES.Payload payload)
    {
        print($"received : [{payload.name}] of type [{payload.GetType()}]");
        switch (payload)
        {
            case USES.Payloads.StringPayload stringPayload:
                print($"[{stringPayload.Data}]");
                break;
            default:
                print($"Empty Payload");
                break;
        }
    }
}