using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtCam : MonoBehaviour
{
    public bool additional;

    private float _dist = .1f;

    void Update()
    {
        transform.eulerAngles = new Vector3(0, 0, transform.eulerAngles.z);
        if (additional)
            transform.position = transform.parent.position + Vector3.back * _dist;
    }
}