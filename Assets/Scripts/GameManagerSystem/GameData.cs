﻿using System.Collections.Generic;
using GameLogic;
using GamevrestUtils;
using SaveSystem;
using UnityEngine;

namespace GameManagerSystem
{
    public class GameData : MonoBehaviour
    {
        [Header("Save Data")] [ReadOnly] public SaveData savedData;
        [Header("Game Data")] public int score;
        public List<Task> tasks = new List<Task>();
        public List<Task> waitingGugus = new List<Task>();
        public List<Task> servingGugus = new List<Task>();
        public float timer;

        private void Awake()
        {
            Load();
        }

        public void New()
        {
            savedData = new SaveData {scores = new List<Score>()};
        }

        public void Save() => SaveManager.Save(savedData);

        public void Load() => savedData = SaveManager.Load();

        public Task GetTaskFromId(int id) => tasks.Find(t => t.id == id);
    }
}