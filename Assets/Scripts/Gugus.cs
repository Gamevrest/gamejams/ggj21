using System.Collections;
using System.Collections.Generic;
using GameLogic;
using UnityEngine;
using Color = UnityEngine.Color;

public class Gugus : MonoBehaviour
{
    public Task task;
    
    public Color[] bodyColors;
    public Color[] topColors;
    public Color[] middleColors;
    public Color[] bottomColors;
    public Sprite[] topSprites;
    public Sprite[] middleSprites;
    public Sprite[] bottomSprites;

    public GameObject top;
    public GameObject middle;
    public GameObject bottom;
    public GameObject body;
    
    void Start()
    {
        Randomize();
    }

    void Randomize()
    {
        body.GetComponent<SpriteRenderer>().color = bodyColors[Random.Range(0, bodyColors.Length)];
        top.GetComponent<SpriteRenderer>().sprite = topSprites[Random.Range(0, topSprites.Length)];
        top.GetComponent<SpriteRenderer>().color = topColors[Random.Range(0, topColors.Length)];
        middle.GetComponent<SpriteRenderer>().sprite = middleSprites[Random.Range(0, middleSprites.Length)];
        middle.GetComponent<SpriteRenderer>().color = middleColors[Random.Range(0, middleColors.Length)];
        bottom.GetComponent<SpriteRenderer>().sprite = bottomSprites[Random.Range(0, bottomSprites.Length)];
        bottom.GetComponent<SpriteRenderer>().color = bottomColors[Random.Range(0, bottomColors.Length)];
    }
}
