﻿using System;
using System.Collections.Generic;
using GameManagerSystem;
using TMPro;
using UnityEngine;

namespace GameLogic
{
    public class DialogManager : MonoBehaviour
    {
        public List<TextMeshProUGUI> dialogs = new List<TextMeshProUGUI>();
        private GameData _gameData;
        public TextMeshProUGUI score;

        public void Start()
        {
            if (_gameData == null) _gameData = PersistentObject.GetGameData();
        }

        public void AddCurrentTaskFromGameData()
        {
            if (_gameData == null) _gameData = PersistentObject.GetGameData();
            for (var i = 0; i < dialogs.Count; i++)
            {
                if (i < _gameData.servingGugus.Count)
                    SetDialog(dialogs[i], _gameData.servingGugus[i]);
                else
                    dialogs[i].transform.parent.gameObject.SetActive(false);
            }
        }

        public void SetDialog(TextMeshProUGUI dialog, Task task)
        {
            dialog.transform.parent.gameObject.SetActive(true);
            dialog.text = task.ToString();
        }

        public void Update()
        {
            score.text = "Score : " + _gameData.score;
        }
    }
}