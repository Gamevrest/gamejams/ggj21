﻿using System.Collections.Generic;
using GameManagerSystem;
using GamevrestUtils;
using UnityEngine;
using Random = UnityEngine.Random;

namespace GameLogic
{
    public class SpawnerV2 : MonoBehaviour
    {
        [SerializeField] private float intervalBetweenSpawn = .5f;
        [SerializeField] private Transform container;
        [SerializeField] private Transform spawnPosition;
        [SerializeField] private GameObject[] shapes;
        public Queue<Task> objectsToSpawn = new Queue<Task>();
        [ReadOnly] public Queue<GameObject> availableBalls = new Queue<GameObject>();
        private GameData _gameData;
        public float spawnForceMin = 1f;
        public float spawnForceMax = 100f;

        public void AddOneFromGameData(USES.Payload payload)
        {
            if (payload is USES.Payloads.IntPayload taskId)
            {
                var obj =
                    (_gameData.GetTaskFromId(taskId.Data) ?? _gameData.waitingGugus.Find(t => t.id == taskId.Data)) ??
                    _gameData.servingGugus.Find(t => t.id == taskId.Data);
                objectsToSpawn.Enqueue(obj);
            }
        }

        public void FetchFromGameData(USES.Payload payload)
        {
            if (_gameData == null) _gameData = PersistentObject.GetGameData();
            foreach (var task in _gameData.tasks)
                objectsToSpawn.Enqueue(task);
            StartCheckForBallToSpawn();
        }

        public void SpawnBall()
        {
            if (objectsToSpawn.Count <= 0) return;
            var task = objectsToSpawn.Dequeue();
            var ball = availableBalls.Count <= 0 ? CreateNewBall() : availableBalls.Dequeue();
            ball.transform.position = spawnPosition.position;
            var randomRot = new Vector3(Random.Range(0f, 360f), Random.Range(0f, 360f), Random.Range(0f, 360f));
            ball.transform.rotation = Quaternion.Euler(randomRot);
            var randomForce = new Vector3(Random.Range(spawnForceMin, spawnForceMax),
                Random.Range(spawnForceMin, spawnForceMax), Random.Range(spawnForceMin, spawnForceMax));
            var ballRigid = ball.GetComponent<Rigidbody>();
            ballRigid.AddForce(randomForce);
            var ballScript = ball.GetComponent<BallScript>();
            ballScript.taskId = task.id;
            ballScript.mainSprite.sprite = task.mainObject.sprite;
            ballScript.mainSprite.color = task.color.color;
            ballScript.addSprite.sprite = task.addObject?.sprite;
            ball.name = task.id.ToString();
            ball.SetActive(true);
        }

        private GameObject CreateNewBall()
        {
            var randomShape = Random.Range(0, shapes.Length);
            var newBall = Instantiate(shapes[randomShape], container);
            return newBall;
        }

        public void StartCheckForBallToSpawn(USES.Payload payload = null) =>
            InvokeRepeating(nameof(SpawnBall), 0, intervalBetweenSpawn);

        public void StopSpawnBalls(USES.Payload payload) => CancelInvoke();
    }
}