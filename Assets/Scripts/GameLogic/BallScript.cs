using System;
using GamevrestUtils;
using UnityEngine;

namespace GameLogic
{
    public class BallScript : MonoBehaviour
    {
        public USES.Event spawnAgain;
        public USES.Event dropInBasket;
        public SpriteRenderer mainSprite;
        public SpriteRenderer addSprite;
        [ReadOnly] public int taskId;
        public Vector2 pivot;

        public float respawnLimit = -10;
        private Camera _mainCam;
        private Rigidbody _rigidBody;

        private void Update()
        {
            if (!(transform.position.y < respawnLimit)) return;
            spawnAgain.Raise(taskId);
            Destroy(gameObject);
        }

        private void OnMouseDown()
        {
            if (_rigidBody == null) _rigidBody = GetComponent<Rigidbody>();
            _rigidBody.velocity = Vector3.zero;
        }

        private void OnMouseDrag()
        {
            if (_mainCam == null) _mainCam = Camera.main;
            var mousePosWorld = _mainCam.ScreenToWorldPoint(Input.mousePosition);
            transform.position =
                new Vector3(mousePosWorld.x + pivot.x, mousePosWorld.y + pivot.y, transform.position.z);
        }

        private void OnCollisionEnter(Collision collision)
        {
            if (collision.transform.CompareTag("basket"))
            {
                dropInBasket.Raise(taskId);
                //spawnAgain.Raise(taskId);
                Destroy(gameObject);
            }
        }
    }
}