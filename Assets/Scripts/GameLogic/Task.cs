﻿using System;
using UnityEngine;

namespace GameLogic
{
    [Serializable]
    public class Color
    {
        public string name;
        public Color32 color;
    }


    [Serializable]
    public class Object
    {
        public string name;
        public Sprite sprite;
        public int palette;
    }

    [Serializable]
    public class Task
    {
        public int id;
        public Object mainObject;
        public Color color;
        public Object addObject;

        public override string ToString()
        {
            var text =
                $"I lost a <color=#{ColorUtility.ToHtmlStringRGBA(color.color)}>{color.name}</color> <b>{mainObject.name}</b>";
            if (addObject != null && addObject.sprite != null && !String.IsNullOrWhiteSpace(addObject.name))
                text += $" with a <b>{addObject.name}</b>";
            return text;
        }

        public bool IsEquals(Task task) =>
            task.color == color && task.mainObject == mainObject && task.addObject == addObject;
    }
}