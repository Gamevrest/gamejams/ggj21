﻿using System;
using GameManagerSystem;
using GamevrestUtils;
using UnityEngine;
using UnityEngine.PlayerLoop;
using USES;
using USES.Payloads;
using Random = UnityEngine.Random;

namespace GameLogic
{
    public class GameManager : MonoBehaviour
    {
        public USES.Event startGenerating;
        public USES.Event endGame;
        public USES.Event selectTask;
        public USES.Event generateGugus;
        public USES.Event spawnTask;
        private GameData _gameData;
        public USES.Event generateOneTask;
        public USES.Event failTask;
        public USES.Event succesTask;
        public float generateOneTaskFrequency = 1f;
        public float decreaseGugusFrequency = 0.05f;
        public float generateGugusFrequency = 1f;
        public float refreshFrequency = 0.2f;
        public float initialGugusNumber = 3;
        public float maxWaitingGugus = 30;
        public int maxTasks = 30;
        private float timer;
        private bool isplaying;
        private bool reinvocation = false;
        private void Start() => Init();

        public void Init()
        {
            if (_gameData == null) _gameData = PersistentObject.GetGameData();
            _gameData.servingGugus.Clear();
            _gameData.waitingGugus.Clear();
            startGenerating.Raise();
        }

        public void StartGame()
        {
            if (_gameData == null) _gameData = PersistentObject.GetGameData();
            _gameData.score = 0;
            isplaying = true;
            timer = Time.timeSinceLevelLoad;
            InvokeRepeating(nameof(GenerateOneTask), generateOneTaskFrequency, generateOneTaskFrequency);
            for (var i = 0; i < initialGugusNumber; i++)
                GenerateGugus();
            reinvocation = true;
            Invoke(nameof(GenerateGugus), generateGugusFrequency);
            InvokeRepeating(nameof(SelectNewTask), 0, refreshFrequency);
        }

        public void GenerateOneTask()
        {
            if (_gameData == null) _gameData = PersistentObject.GetGameData();
            if (_gameData.tasks.Count + _gameData.waitingGugus.Count + _gameData.servingGugus.Count >= maxTasks)
                return;
            generateOneTask.Raise();
        }

        public void GenerateGugus()
        {
            if (_gameData == null) _gameData = PersistentObject.GetGameData();
            Debug.Log("before check size");
            // if (_gameData.waitingGugus.Count + _gameData.servingGugus.Count >= _gameData.tasks.Count)
            //     return;
            Debug.Log("before select gugus");
            var task = new Task {color = null};
            for (var i = 0; i < _gameData.tasks.Count; i++)
            {
                var taskId = Random.Range(0, _gameData.tasks.Count);
                task = _gameData.tasks[taskId];
                if (_gameData.waitingGugus.Find(t => t.id == task.id) == null)
                    break;
                task = new Task {color = null};
            }

            Debug.Log("before add gugus");
            if (task.color != null)
            {
                _gameData.waitingGugus.Add(task);
                _gameData.tasks.Remove(task);
                generateGugus.Raise(task.id);
            }

            Debug.Log("before reinvoc");
            if (!reinvocation)
                return;
            if (generateGugusFrequency > 0.3f)
                generateGugusFrequency -= decreaseGugusFrequency;
            Invoke(nameof(GenerateGugus), generateGugusFrequency);
        }


        public void SelectNewTask()
        {
            if (_gameData == null) _gameData = PersistentObject.GetGameData();

            while (_gameData.servingGugus.Count < 3 && _gameData.waitingGugus.Count > 0)
            {
                var taskId = Random.Range(0, _gameData.waitingGugus.Count);
                var task = _gameData.waitingGugus[taskId];
                _gameData.servingGugus.Add(task);
                _gameData.waitingGugus.Remove(task);
            }

            selectTask.Raise();
        }

        private void CheckTask(Task task)
        {
            foreach (var servingGugus in _gameData.servingGugus)
            {
                if (task.IsEquals(servingGugus))
                {
                    succesTask.Raise();
                    Debug.Log("marvelous");
                    _gameData.score += 2;
                    _gameData.servingGugus.Remove(servingGugus);
                    _gameData.tasks.Remove(_gameData.GetTaskFromId(servingGugus.id));
                    //SelectNewTask();
                    return;
                }
            }

            failTask.Raise();
            Debug.Log("t movais");
            _gameData.score -= 1;
            spawnTask.Raise(task.id);
        }

        public void VerifyTask(Payload payload)
        {
            if (_gameData == null) _gameData = PersistentObject.GetGameData();
            if (_gameData.servingGugus.Count <= 0) return;
            if (payload is USES.Payloads.IntPayload taskId)
            {
                Task task;
                if (taskId.Data == -1)
                    task = _gameData.servingGugus[0];
                else
                    task = (_gameData.GetTaskFromId(taskId.Data) ??
                            _gameData.waitingGugus.Find(t => t.id == taskId.Data)) ??
                           _gameData.servingGugus.Find(t => t.id == taskId.Data);
                CheckTask(task);
            }

            TestIfGameFinished();
        }

        void Update()
        {
            if (isplaying)
                TestIfGameFinished();
        }

        private bool TestIfGameFinished()
        {
            if (_gameData.waitingGugus.Count + _gameData.servingGugus.Count >= maxWaitingGugus)
            {
                isplaying = false;
                _gameData.timer = Time.timeSinceLevelLoad - timer;
                CancelInvoke();
                endGame.Raise();
                return true;
            }

            return false;
        }
    }
}