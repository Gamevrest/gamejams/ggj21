﻿using System;
using UnityEngine;

namespace GameLogic
{
    public class MusicManager : MonoBehaviour
    {
        public AudioClip succes;
        public AudioClip failure;
        public AudioClip spawn;
        private AudioSource _audioSource;

        private void Awake()
        {
            _audioSource = GetComponent<AudioSource>();
        }

        public void Success()
        {
            _audioSource.PlayOneShot(succes);
        }
        public void Failure()
        {
            _audioSource.PlayOneShot(failure);
        }
        
        public void Spawn()
        {
            _audioSource.PlayOneShot(spawn);
        }
    }
}