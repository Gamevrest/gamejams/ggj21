﻿using System;
using System.Collections.Generic;
using System.Linq;
using GameManagerSystem;
using GamevrestUtils;
using UnityEngine;

namespace GameLogic
{
    public class GuGusManager : MonoBehaviour
    {
        public GameObject gugus;
        public Transform gugusContainer;
        public List<Transform> positions = new List<Transform>();
        public List<Transform> gugusses = new List<Transform>();
        public float intervalBetweenSpawn;
        public int gugusToSpawn;
        private GameData _gameData;
        public float decal = .1f;

        private void Start()
        {
            gugusses.Clear();
            foreach (Transform child in gugusContainer)
                positions.Add(child);
        }

        public void AddOneGugus()
        {
            gugusToSpawn++;
        }

        public void FetchFromGameData()
        {
            if (_gameData == null) _gameData = PersistentObject.GetGameData();
            foreach (var gugus in gugusses)
                Destroy(gugus.gameObject);
            gugusses.Clear();
            gugusToSpawn = _gameData.waitingGugus.Count + _gameData.servingGugus.Count;
            StartCheckForGugusToSpawn();
        }

        public void SpawnGugus()
        {
            if (gugusses.Count < positions.Count)
            {
                if (gugusToSpawn > 0)
                {
                    var obj = Instantiate(gugus, gugusContainer);
                    gugusses.Add(obj.transform);
                    gugusToSpawn--;
                }
                for (var i = 0; i < gugusses.Count; i++)
                    gugusses[i].position = positions[i].position + Vector3.back * i * decal;
            }
        }

        public void StartCheckForGugusToSpawn() =>
            InvokeRepeating(nameof(SpawnGugus), 0, intervalBetweenSpawn);

        public void StopCheckGugus()
        {
            Debug.Log("stop spawn gugus");
            gugusToSpawn = 0;
            foreach (var gugus in gugusses)
                Destroy(gugus.gameObject);
            gugusses.Clear();
            CancelInvoke();
        }

        public void RemoveGugusHappy()
        {
            //play anim gugus happy
            RemoveGugus();
        }

        public void RemoveGugusSad()
        {
            //play anim gugus sad
            //RemoveGugus();
        }

        public void RemoveGugus()
        {
            var obj = gugusses.First();
            gugusses.Remove(obj);
            Destroy(obj.gameObject);
        }
    }
}