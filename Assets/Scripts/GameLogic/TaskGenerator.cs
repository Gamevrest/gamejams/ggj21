﻿using System;
using System.Collections.Generic;
using GameManagerSystem;
using UnityEngine;
using Random = UnityEngine.Random;

namespace GameLogic
{
    [Serializable]
    public class Palette
    {
        public List<Color> colors = new List<Color>();
    }

    public class TaskGenerator : MonoBehaviour
    {
        [Header("Setup Objects")] [Range(0, 100)]
        public float chanceAddObject = 50;

        public List<Palette> palettes = new List<Palette>();
        public List<Object> mainObjects = new List<Object>();
        public List<Object> addObjects = new List<Object>();
        [Header("Setup Generator")] public int nbTaskToGenerate = 10;
        private GameData _gameData;
        public USES.Event finishedGeneration;
        public USES.Event addNewToSpawner;
        public int maxid;
        public void GenerateTasks(USES.Payload payload)
        {
            if (_gameData == null) _gameData = PersistentObject.GetGameData();
            _gameData.tasks.Clear();
            for (var i = -1; i < nbTaskToGenerate - 1; ++i)
                GenerateTask(i);
            maxid = _gameData.tasks.Count + 1;
            finishedGeneration.Raise();
        }

        public int GenerateTask(int i)
        {
            if (_gameData == null) _gameData = PersistentObject.GetGameData();
            var randomMainObject = Random.Range(0, mainObjects.Count);
            var mainObject = mainObjects[randomMainObject];
            var randomColor = Random.Range(0, palettes[mainObject.palette].colors.Count);

            var task = new Task
            {
                id = i + 1,
                color = palettes[mainObject.palette].colors[randomColor],
                mainObject = mainObject
            };
            if (Random.Range(0, 100) <= chanceAddObject)
            {
                var randomAddObject = Random.Range(0, addObjects.Count);
                task.addObject = addObjects[randomAddObject];
            }

            _gameData.tasks.Add(task);
            return task.id;
        }

        public void GenerateAddTask()
        {
            
            if (_gameData == null) _gameData = PersistentObject.GetGameData();
            var id = GenerateTask(maxid++);
            addNewToSpawner.Raise(id);
        }
    }
}