﻿using System;
using System.Globalization;
using GameManagerSystem;
using TMPro;
using UnityEngine;

namespace GameLogic
{
    public class UIManager : MonoBehaviour
    {
        public TextMeshProUGUI score;
        public TextMeshProUGUI time;
        public RectTransform resultContainer;

        private GameData _gameData;

        public void DisplayResult()
        {
            if (_gameData == null) _gameData = PersistentObject.GetGameData();
            score.text = _gameData.score.ToString();
            var ts = TimeSpan.FromSeconds(_gameData.timer);
            time.text = $"{ts.TotalMinutes:00}:{ts.Seconds:00}";
            resultContainer.gameObject.SetActive(true);
        }
    }
}