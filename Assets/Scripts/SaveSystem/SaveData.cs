﻿using System;
using System.Collections.Generic;

namespace SaveSystem
{
    [Serializable]
    public class Score
    {
        public string name;
        public int score;
    }

    [Serializable]
    public class SaveData
    {
        public List<Score> scores;
    }
}